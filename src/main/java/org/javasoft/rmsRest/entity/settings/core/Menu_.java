package org.javasoft.rmsRest.entity.settings.core;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.settings.core.Functions;
import org.javasoft.rmsRest.entity.settings.core.Menu;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Menu.class)
public abstract class Menu_ {

	public static volatile SingularAttribute<Menu, String> flag;
	public static volatile SingularAttribute<Menu, Functions> functions;
	public static volatile SingularAttribute<Menu, String> menuCode;
	public static volatile SingularAttribute<Menu, Long> menuId;
	public static volatile SingularAttribute<Menu, String> menuName;
	public static volatile SingularAttribute<Menu, Date> createDate;

}

