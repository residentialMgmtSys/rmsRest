/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper.group;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.javasoft.rmsLib.dto.group.UserIdentityGroupDTO;
import org.javasoft.rmsRest.entity.group.UserIdentityGroup;
import org.javasoft.rmsRest.mapper.GenericMapper;
import org.javasoft.rmsRest.mapper.user.UserIdentityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Component
public class UserIdentityGroupMapper implements GenericMapper<UserIdentityGroupDTO,UserIdentityGroup>{
    
    @Autowired
    private UserIdentityMapper userIdentityMapper;
    
    @Override
    public List<UserIdentityGroupDTO> convertEntityListToDTOList(Stream<UserIdentityGroup> entities) {
        return entities.map(convertEntityToDTO).collect(Collectors.toList());
    }
    
    @Override
    public UserIdentityGroupDTO convertEntityToDTO (UserIdentityGroup entity){
        return convertEntityToDTO.apply(entity);
    }
    
    private Function<UserIdentityGroup, UserIdentityGroupDTO> convertEntityToDTO=(UserIdentityGroup entity) -> {
            UserIdentityGroupDTO userIdentityGroupDTO = new UserIdentityGroupDTO();
            userIdentityGroupDTO.setCreateDate(entity.getCreateDate());
            userIdentityGroupDTO.setDefaultGrp(entity.isDefaultGrp());
            userIdentityGroupDTO.setFlag(entity.getFlag());
            userIdentityGroupDTO.setGroupCode(entity.getGroupCode());
            userIdentityGroupDTO.setGroupName(entity.getGroupName());
            userIdentityGroupDTO.setGroupsId(entity.getGroupsId());
            userIdentityGroupDTO.setSubMenu(entity.getSubMenu());
            //userIdentityMapper.convertEntityListToVO(entity.getAllUserIdentity().stream());
            return userIdentityGroupDTO;
        };
   
}
