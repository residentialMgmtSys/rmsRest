package org.javasoft.rmsRest.entity.account;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.account.ResidentAccount;
import org.javasoft.rmsRest.entity.occupants.Resident;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ResidentAccount.class)
public abstract class ResidentAccount_ extends org.javasoft.rmsRest.entity.account.AbstractAccount_ {

	public static volatile SingularAttribute<ResidentAccount, Resident> resident;

}

