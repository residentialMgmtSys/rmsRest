package org.javasoft.rmsRest.entity.group;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.facility.EstateUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EstateUnitGroup.class)
public abstract class EstateUnitGroup_ extends org.javasoft.rmsRest.entity.group.Groups_ {

	public static volatile ListAttribute<EstateUnitGroup, EstateUnit> estateUnits;

}

