/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.validation.ConstraintViolation;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.javasoft.rmsLib.utils.error.ErrorDetail;
import org.javasoft.rmsLib.utils.error.SubErrors;
import org.springframework.validation.FieldError;

/**
 *
 * @author ayojava
 */
public class ErrorBuilder {
    
    private ErrorDetail errorDetail;
    
    private List<SubErrors> subErrorsList;
    
    @PostConstruct
    public void init(){
        errorDetail = new ErrorDetail();
        errorDetail.setDate(new Date());
        subErrorsList = new ArrayList<>();        
    }
    
    public void setErrorDetail(int httpStatusCode,String title, String detail,String message){
        errorDetail.setTitle(title);
        errorDetail.setDetail(detail);
        errorDetail.setMessage(message);
        errorDetail.setHttpStatusCode(httpStatusCode);
    }
    
    public void addConstraintViolationErrors(Set<ConstraintViolation<?>> cViolations){
        cViolations.forEach((cViolation)->{
            subErrorsList.add(new SubErrors(cViolation.getRootBeanClass().getSimpleName(),
                    ((PathImpl) cViolation.getPropertyPath()).getLeafNode().asString(),cViolation.getInvalidValue(),
                    cViolation.getMessage()));
        });
    }
    
    public void addFieldValidationErrors(List<FieldError> fieldErrors){
        fieldErrors.forEach((aFieldError)->{
            subErrorsList.add(new SubErrors(aFieldError.getObjectName(),aFieldError.getField(),aFieldError.getRejectedValue(),
                    aFieldError.getDefaultMessage()));
        });    
    }

    public ErrorDetail getErrorDetail() {
        errorDetail.setSubErrorsList(subErrorsList);
        return errorDetail;
    }
    
    
    @PreDestroy
    public void destroy(){
        errorDetail = null;
        subErrorsList = null;
    }
}
