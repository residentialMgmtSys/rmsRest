/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.service.settings.core;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;
import org.javasoft.rmsRest.mapper.settings.core.ProfileMapper;
import org.javasoft.rmsRest.repository.settings.core.ProfileRepository;
import static org.javasoft.rmsLib.utils.flag.GenericFlagIntf.ACTIVE;
import org.javasoft.rmsRest.service.CRUDServiceIntf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ayojava
 */
@Slf4j
@Service
@Transactional
public class ProfileService implements CRUDServiceIntf<ProfileDTO>{
    
    private final ProfileRepository profileRepository;
    
    @Autowired
    private ProfileMapper profileMapper; 
    
    @Autowired
    public ProfileService(ProfileRepository profileRepository){
        this.profileRepository = profileRepository;
    }
    
    public List<ProfileDTO> findAllActiveProfiles(){
        return profileMapper.convertEntityListToDTOList(profileRepository.findByFlagOrderByProfileNameAsc(ACTIVE));
    }
    
    public List<ProfileDTO> findAll(){
        return profileMapper.convertEntityListToDTOList(profileRepository.findAllBy());
    }

    @Override
    public ProfileDTO getById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
