/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.mail;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.javasoft.rmsRest.entity.occupants.Resident;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "EmailRecipients")
public class EmailRecipients implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "EmailRecipientsID_GEN", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "EmailRecipientsID_GEN", sequenceName = "EmailRecipientsID_SEQ")
    private Long emailRecipientsId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "residentId")
    private Resident recipient;
    
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    @JoinColumn(name = "emailId")
    private Email email ;
    
    private String flag ;//OutEmail [sent , pending  ] , InEmail [read , unread ]
    
    private String recipentType; //CC, TO ,BCC
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @CreationTimestamp
    private Date createDate;
}
