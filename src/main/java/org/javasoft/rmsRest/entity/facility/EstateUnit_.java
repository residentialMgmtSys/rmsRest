package org.javasoft.rmsRest.entity.facility;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.account.EstateUnitAccount;
import org.javasoft.rmsRest.entity.flow.EntityState;
import org.javasoft.rmsRest.entity.group.EstateUnitGroup;
import org.javasoft.rmsRest.entity.user.UserIdentity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EstateUnit.class)
public abstract class EstateUnit_ {

	public static volatile SingularAttribute<EstateUnit, String> unitType;
	public static volatile ListAttribute<EstateUnit, EstateUnitGroup> estateUnitGroups;
	public static volatile SingularAttribute<EstateUnit, Integer> unitSequence;
	public static volatile SingularAttribute<EstateUnit, EstateUnitAccount> estateUnitAccount;
	public static volatile SingularAttribute<EstateUnit, Integer> groupCount;
	public static volatile SingularAttribute<EstateUnit, EntityState> entityState;
	public static volatile SingularAttribute<EstateUnit, Long> estateUnitId;
	public static volatile SingularAttribute<EstateUnit, Long> actualEstateUnitId;
	public static volatile SingularAttribute<EstateUnit, String> status;
	public static volatile SingularAttribute<EstateUnit, Date> createDate;
	public static volatile ListAttribute<EstateUnit, UserIdentity> userIdentitys;

}

