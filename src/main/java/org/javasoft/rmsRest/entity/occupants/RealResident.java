/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.occupants;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.javasoft.rmsRest.entity.facility.ResidentialUnit;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@Table(name = "RealResident")
@EqualsAndHashCode(callSuper = true)
public class RealResident extends Resident implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "RealResident_ResidentialUnit" ,
            joinColumns = @JoinColumn(name = "residentId" ,nullable = false ,referencedColumnName = "residentId"),
            inverseJoinColumns = @JoinColumn(name="residentialUnitId" ,nullable = false ,referencedColumnName = "estateUnitId"))
    private ResidentialUnit residentialUnit;
}
