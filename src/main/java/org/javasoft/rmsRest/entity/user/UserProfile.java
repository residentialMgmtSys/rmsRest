/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.user;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.javasoft.rmsRest.entity.settings.core.Menu;
import org.javasoft.rmsRest.entity.settings.core.Module;
import org.javasoft.rmsRest.entity.settings.core.Profile;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "UserProfile")
public class UserProfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userProfileId;

    @OneToOne()
    private Profile profile;
    
    @OneToOne()
    private Module module;
    
    @OneToOne()
    private Menu menu;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false,name="createDate")
    @CreationTimestamp
    private Date createDate;

    
}
