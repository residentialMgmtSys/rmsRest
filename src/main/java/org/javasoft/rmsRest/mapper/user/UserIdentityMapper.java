/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper.user;

import java.util.ArrayList;
import org.javasoft.rmsLib.dto.user.UserIdentityDTO;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.javasoft.rmsLib.dto.facility.EstateUnitDTO;
import org.javasoft.rmsLib.dto.flow.EntityStateDTO;
import org.javasoft.rmsLib.dto.group.UserIdentityGroupDTO;
import org.javasoft.rmsLib.dto.occupants.ResidentDTO;
import org.javasoft.rmsLib.dto.user.UserProfileDTO;
import org.javasoft.rmsRest.entity.facility.EstateUnit;
import org.javasoft.rmsRest.entity.flow.EntityState;
import org.javasoft.rmsRest.entity.group.UserIdentityGroup;
import org.javasoft.rmsRest.entity.occupants.Resident;
import org.javasoft.rmsRest.entity.user.UserIdentity;
import org.javasoft.rmsRest.entity.user.UserProfile;
import org.javasoft.rmsRest.mapper.GenericMapper;
import org.javasoft.rmsRest.mapper.facility.EstateUnitMapper;
import org.javasoft.rmsRest.mapper.flow.EntityStateMapper;
import org.javasoft.rmsRest.mapper.group.UserIdentityGroupMapper;
import org.javasoft.rmsRest.mapper.occupants.ResidentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Component
public class UserIdentityMapper implements GenericMapper<UserIdentityDTO,UserIdentity>{

    @Autowired
    ResidentMapper residentMapper;

    @Autowired
    UserIdentityGroupMapper userIdentityGroupMapper;

    @Autowired
    EstateUnitMapper estateUnitMapper;

    @Autowired
    EntityStateMapper entityStateMapper;

    @Autowired
    UserProfileMapper userProfileMapper;

    public boolean eagerFetch;

    @Override
    public List<UserIdentityDTO> convertEntityListToDTOList(Stream<UserIdentity> entities) {
        return entities.map(convertEntityToDTO).collect(Collectors.toList());
    }

    @Override
    public UserIdentityDTO convertEntityToDTO(UserIdentity entity){
        return convertEntityToDTO.apply(entity);
    }
    
    private Function<UserIdentity, UserIdentityDTO> convertEntityToDTO=(UserIdentity entity) -> {
            UserIdentityDTO userIdentityDTO = new UserIdentityDTO();
            userIdentityDTO.setFlag(entity.getFlag());
            userIdentityDTO.setStatus(entity.getStatus());
            userIdentityDTO.setSystemNo(entity.getSystemNo());
            userIdentityDTO.setUserName(entity.getUserName());
            userIdentityDTO.setUserIdentityId(entity.getUserIdentityId());
            userIdentityDTO.setCreateDate(entity.getCreateDate());
            userIdentityDTO.setSysAdmin(entity.isSysAdmin());

            Optional<Resident> residentOptional = Optional.ofNullable(entity.getResidentDetails());
            Optional<EstateUnit> estateUnitOptional = Optional.of(entity.getEstateUnit());
            Optional<EntityState> entityStateOptional = Optional.of(entity.getEntityState());
//residentMapper.convertEntityToDTO.apply(resident)
            ResidentDTO residentDTO = residentOptional.flatMap((Resident resident) -> {
                return Optional.of(residentMapper.convertEntityToDTO(resident));
            }).orElse(new ResidentDTO(false));

            EstateUnitDTO estateUnitDTO = estateUnitOptional.flatMap((EstateUnit estateUnit) -> {
                return Optional.of(estateUnitMapper.convertEntityToDTO(estateUnit));
            }).orElse(new EstateUnitDTO(false));

            EntityStateDTO entityStateDTO = entityStateOptional.flatMap((EntityState entityState) -> {
                return Optional.of(entityStateMapper.convertEntityToDTO(entityState));
            }).orElse(new EntityStateDTO(false));

            if (eagerFetch) {
                List<UserIdentityGroup> userIdentityGroup = entity.getUserIdentityGroups();
                List<UserIdentityGroupDTO> userIdentityGroupDTOs = 
                        (userIdentityGroup == null || userIdentityGroup.isEmpty()) ? new ArrayList<>() : userIdentityGroupMapper.convertEntityListToDTOList(userIdentityGroup.stream());
                userIdentityDTO.setUserIdentityGroups(userIdentityGroupDTOs);
            } else {
                userIdentityDTO.setUserIdentityGroups(new ArrayList<>());
            }

            List<UserProfile> userProfileList = entity.getSystemUserProfile();
            List<UserProfileDTO> UserProfileDTOs = (userProfileList == null || userProfileList.isEmpty()) ? new ArrayList<>() : userProfileMapper.convertEntityListToDTOList(userProfileList.stream());

            userIdentityDTO.setEntityState(entityStateDTO);
            userIdentityDTO.setEstateUnit(estateUnitDTO);
            userIdentityDTO.setResidentDetails(residentDTO);
            userIdentityDTO.setUserProfileDTOs(UserProfileDTOs);

            return userIdentityDTO;
        };
  

    UserIdentityDTO convertEntityToVO(UserIdentity entity) {

        UserIdentityDTO userIdentityDTO = new UserIdentityDTO();
        userIdentityDTO.setFlag(entity.getFlag());
        userIdentityDTO.setStatus(entity.getStatus());
        userIdentityDTO.setSystemNo(entity.getSystemNo());
        userIdentityDTO.setUserName(entity.getUserName());
        userIdentityDTO.setUserIdentityId(entity.getUserIdentityId());

        return userIdentityDTO;
    }

}
