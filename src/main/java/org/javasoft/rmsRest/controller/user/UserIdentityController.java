/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.controller.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rmsRest.service.user.UserIdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.javasoft.rmsLib.dto.user.UserIdentityDTO;
import static org.javasoft.rmsLib.api.user.UserIdentityDTOPath.LIST_USER_IDENTITY_DTO_API;
import static org.javasoft.rmsLib.api.user.UserIdentityDTOPath.LOGIN_API;
import static org.javasoft.rmsLib.api.user.UserIdentityDTOPath.V1_API;
import static org.javasoft.rmsLib.utils.error.ErrorCodes.E030;
import org.javasoft.rmsRest.exception.LoginException;

/**
 *
 * @author ayojava
 */
@Slf4j
@RestController
@RequestMapping(V1_API)
@Api(value = "UserIdentityDTO", description = "Rest API for UserIdentity Controller", tags = "UserIdentityDTO API")
public class UserIdentityController {

    private final UserIdentityService userIdentityService;

    @Autowired
    public UserIdentityController(UserIdentityService userIdentityService) {
        this.userIdentityService = userIdentityService;
    }

    @GetMapping(LOGIN_API)
    @ApiOperation(value = "Return UserIdentity Details from userName and Password", response = UserIdentityDTO.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "Resource Not Found"),
        @ApiResponse(code = 500, message = "An Exception has occurred during Login")
    })
    public UserIdentityDTO login(@PathVariable String userName, @PathVariable String password) {
        UserIdentityDTO userIdentityDTO = userIdentityService.login(userName, password);
        if (userIdentityDTO == null) throw new LoginException(E030);
        return userIdentityDTO;
    }

    @GetMapping(LIST_USER_IDENTITY_DTO_API)
    @ApiOperation(value = "Return List of all userIdentities in the Database", response = Iterable.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
    })
    public List<UserIdentityDTO> findAll() {
        return userIdentityService.findAll();
    }

}
