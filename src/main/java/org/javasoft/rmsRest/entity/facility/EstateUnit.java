/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.facility;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Formula;
import org.javasoft.rmsRest.entity.account.EstateUnitAccount;
import org.javasoft.rmsRest.entity.flow.EntityState;
import org.javasoft.rmsRest.entity.group.EstateUnitGroup;
import org.javasoft.rmsRest.entity.user.UserIdentity;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "EstateUnit")
@Inheritance(strategy = InheritanceType.JOINED)
public class EstateUnit  implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long estateUnitId;
    
    protected Long actualEstateUnitId;
    
    @Column(name="unitType")
    private String unitType;

    private String status;//AVAILABLE. OCCUPIED . DAMAGED . MOVED OUT

//    @Formula("(select count(*) from EstateUnitGroup_EstateUnit g where g.estateUnit_id = estate_unit_id)")
//    private int groupCount;
    
    @Column(name="unitSequence")
    protected int unitSequence;
        
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false,name="createDate")
    @CreationTimestamp
    private Date createDate;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "estateUnitAccountId", nullable = false)
    @Cascade({org.hibernate.annotations.CascadeType.PERSIST})
    private EstateUnitAccount estateUnitAccount;
    
    @ManyToMany(mappedBy = "estateUnits")
    @Cascade({org.hibernate.annotations.CascadeType.MERGE})
    private List<EstateUnitGroup> estateUnitGroups;
    
    @JoinColumn(name = "entityStateId", referencedColumnName = "entityStateId", nullable = false)
    @OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @Fetch(value = FetchMode.SELECT)
    private EntityState entityState; // 
    
    @OneToMany(mappedBy = "estateUnit")
    private List<UserIdentity> userIdentitys;
        
    @Formula("(select count(*) from EstateUnitGroup_EstateUnit g where g.estateUnitId = estateUnitId)")
    private int groupCount;

    
    
}
