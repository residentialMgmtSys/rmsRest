package org.javasoft.rmsRest.entity.group;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.group.UserIdentityGroup;
import org.javasoft.rmsRest.entity.user.UserIdentity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserIdentityGroup.class)
public abstract class UserIdentityGroup_ extends org.javasoft.rmsRest.entity.group.Groups_ {

	public static volatile ListAttribute<UserIdentityGroup, UserIdentity> allUserIdentity;

}

