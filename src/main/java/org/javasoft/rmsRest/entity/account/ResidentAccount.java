/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.account;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.javasoft.rmsRest.entity.occupants.Resident;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "ResidentAccount")
@EqualsAndHashCode(callSuper = true)
public class ResidentAccount extends AbstractAccount implements Serializable {

    @OneToOne(mappedBy = "residentAccount")
    private Resident resident;
    
    
}
