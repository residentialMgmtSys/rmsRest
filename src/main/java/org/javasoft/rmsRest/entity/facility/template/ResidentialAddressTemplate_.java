package org.javasoft.rmsRest.entity.facility.template;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.facility.template.ResidentialAddressTemplate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ResidentialAddressTemplate.class)
public abstract class ResidentialAddressTemplate_ {

	public static volatile SingularAttribute<ResidentialAddressTemplate, Long> blockNum;
	public static volatile SingularAttribute<ResidentialAddressTemplate, Long> flatNum;
	public static volatile SingularAttribute<ResidentialAddressTemplate, String> zoneCode;

}

