/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.occupants;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.javasoft.rmsRest.entity.facility.NonResidentialUnit;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@Table(name = "VirtualResident")
@EqualsAndHashCode(callSuper = true)
public class VirtualResident extends Resident implements Serializable {

    @Column(name="contactAddress")
    private String contactAddress;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "VirtualResident_NonResidentialUnit" ,
            joinColumns = @JoinColumn(name = "residentId" ,nullable = false ,referencedColumnName = "residentId"),
            inverseJoinColumns = @JoinColumn(name="nonResidentialUnitId" ,nullable = false ,referencedColumnName = "estateUnitId"))
    private NonResidentialUnit nonResidentialUnit;
}
