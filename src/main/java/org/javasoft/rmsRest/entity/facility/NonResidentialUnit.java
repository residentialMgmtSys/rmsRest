/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.facility;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.javasoft.rmsRest.entity.facility.template.NonResidentialAddressTemplate;
import org.javasoft.rmsRest.entity.occupants.VirtualResident;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@Table(name = "NonResidentialUnit")
@EqualsAndHashCode(callSuper = true)
public class NonResidentialUnit extends EstateUnit implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Column(name="nonResidentialType")
    private String nonResidentialType;//CONTAINER, SHOP
    
    private String activity;//
    
    @Column
    @Embedded
    private NonResidentialAddressTemplate nonResidentialAddressTemplate;
    
    @OneToMany(mappedBy = "nonResidentialUnit",fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    private List<VirtualResident> virtualResidents;
    
    @Override
    public void setUnitSequence(int unitSequence){
        StringBuilder builder = new StringBuilder();
        builder = builder.append(nonResidentialAddressTemplate.getIdentificationNo());
        builder = builder.append(nonResidentialAddressTemplate.getUnitNo());
        unitSequence = Integer.parseInt(builder.toString());
    }
    
    
     
    
    
}
