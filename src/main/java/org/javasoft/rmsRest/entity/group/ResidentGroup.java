/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.group;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.javasoft.rmsRest.entity.occupants.Resident;

/**
 *
 * @author ayojava
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ResidentGroup extends Groups implements Serializable {
    
    @ManyToMany()
    @JoinTable(name = "ResidentGroup_Resident",
            joinColumns = @JoinColumn(name = "groupsId" ,referencedColumnName = "groupsId"),
            inverseJoinColumns = @JoinColumn(name = "residentId" ,referencedColumnName = "residentId"))
    private List<Resident> residents;
    
    
}
