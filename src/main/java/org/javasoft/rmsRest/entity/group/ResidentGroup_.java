package org.javasoft.rmsRest.entity.group;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.group.ResidentGroup;
import org.javasoft.rmsRest.entity.occupants.Resident;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ResidentGroup.class)
public abstract class ResidentGroup_ extends org.javasoft.rmsRest.entity.group.Groups_ {

	public static volatile ListAttribute<ResidentGroup, Resident> residents;

}

