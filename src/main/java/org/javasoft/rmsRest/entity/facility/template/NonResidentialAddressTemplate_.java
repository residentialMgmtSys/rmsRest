package org.javasoft.rmsRest.entity.facility.template;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.facility.template.NonResidentialAddressTemplate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NonResidentialAddressTemplate.class)
public abstract class NonResidentialAddressTemplate_ {

	public static volatile SingularAttribute<NonResidentialAddressTemplate, Integer> unitNo;
	public static volatile SingularAttribute<NonResidentialAddressTemplate, String> otherAddress;
	public static volatile SingularAttribute<NonResidentialAddressTemplate, String> identificationNo;
	public static volatile SingularAttribute<NonResidentialAddressTemplate, String> zoneCode;

}

