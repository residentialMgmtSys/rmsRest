/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper.facility;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.javasoft.rmsLib.dto.facility.EstateUnitDTO;
import org.javasoft.rmsRest.entity.facility.EstateUnit;
import org.javasoft.rmsRest.mapper.GenericMapper;
import org.javasoft.rmsRest.mapper.flow.EntityStateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Component
public class EstateUnitMapper implements GenericMapper<EstateUnitDTO,EstateUnit>{
    
    @Autowired
    EntityStateMapper entityStateMapper;
    
    @Override
    public List<EstateUnitDTO> convertEntityListToDTOList(Stream<EstateUnit> entities){
        return entities.map(convertEntityToDTO).collect(Collectors.toList());
    }
    
    @Override
    public EstateUnitDTO convertEntityToDTO (EstateUnit entity){
        return convertEntityToDTO.apply(entity);
    }
    
    private Function<EstateUnit, EstateUnitDTO> convertEntityToDTO = (EstateUnit entity)->{
            EstateUnitDTO estateUnitDTO = new EstateUnitDTO();
            estateUnitDTO.setEstateUnitId(entity.getEstateUnitId());
            estateUnitDTO.setStatus(entity.getStatus());
            estateUnitDTO.setUnitSequence(entity.getUnitSequence());
            estateUnitDTO.setUnitType(entity.getUnitType());
            estateUnitDTO.setActualEstateUnitId(entity.getActualEstateUnitId());
            estateUnitDTO.setCreateDate(entity.getCreateDate());
            
            estateUnitDTO.setEntityState(entityStateMapper.convertEntityToDTO(entity.getEntityState()));
            
            return estateUnitDTO;
        };
}
