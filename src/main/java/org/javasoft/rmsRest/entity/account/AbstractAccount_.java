package org.javasoft.rmsRest.entity.account;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.account.AbstractAccount;
import org.javasoft.rmsRest.entity.account.AccountTemplate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AbstractAccount.class)
public abstract class AbstractAccount_ {

	public static volatile SingularAttribute<AbstractAccount, Long> accountId;
	public static volatile SingularAttribute<AbstractAccount, AccountTemplate> accountTemplate;
	public static volatile SingularAttribute<AbstractAccount, String> flag;
	public static volatile SingularAttribute<AbstractAccount, String> systemAccountNo;
	public static volatile SingularAttribute<AbstractAccount, String> accountName;
	public static volatile SingularAttribute<AbstractAccount, String> accountNo;
	public static volatile SingularAttribute<AbstractAccount, String> balStatus;
	public static volatile SingularAttribute<AbstractAccount, Double> accountBalance;
	public static volatile SingularAttribute<AbstractAccount, Date> createDate;

}

