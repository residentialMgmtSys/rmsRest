/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.jobs;

import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Slf4j
@Component
public class MailJob {
    
    @Scheduled(cron = "0 * * * * ?")
    public void sendPendingMails(){
        //log.info("Mailjob :::: {}", LocalDateTime.now());
    }
}
