/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper.flow;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.javasoft.rmsLib.dto.flow.EntityStateDTO;
import org.javasoft.rmsRest.entity.flow.EntityState;
import org.javasoft.rmsRest.mapper.GenericMapper;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Component
public class EntityStateMapper implements GenericMapper<EntityStateDTO,EntityState>{

    @Override
    public EntityStateDTO convertEntityToDTO (EntityState entity){
        return convertEntityToDTO.apply(entity);
    }
    
    private Function<EntityState, EntityStateDTO> convertEntityToDTO = (EntityState entity) -> {
        return new EntityStateDTO(entity.getEntityStateId(), entity.getStatus());
    };

    @Override
    public List<EntityStateDTO> convertEntityListToDTOList(Stream<EntityState> entities) {
        return entities.map(convertEntityToDTO).collect(Collectors.toList());
    }
}
