/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.facility.template;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author ayojava
 * This class allows for customization of Address for a Residential unit
 * You can mark any field as transient if it's not used OR
 * Attribute Override in the embedded class
 */
@Data
@NoArgsConstructor
@Embeddable
public class ResidentialAddressTemplate implements Serializable {

    @NotNull
    @Column(name="blockNum")
    private Long blockNum;
    
    @NotNull
    @Column(name="flatNum")
    private Long flatNum;
    
    @NotBlank
    @Column(name="zoneCode")
    private String zoneCode ;
    
    
}
