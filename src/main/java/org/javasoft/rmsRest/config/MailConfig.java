/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.config;

import java.util.Properties;
import org.javasoft.rmsRest.property.GMailProperty;
import org.javasoft.rmsRest.qualifiers.MailEnum;
import org.javasoft.rmsRest.qualifiers.MailType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 *
 * @author ayojava
 */
@Configuration
public class MailConfig {
    
    @Autowired
    private GMailProperty gmailProperty;
    
    @Bean
    @Lazy
    @MailType(MailEnum.GMAIL)
    public JavaMailSender getGmailJavaMailSender(){
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(gmailProperty.getHost());
        mailSender.setPassword(gmailProperty.getPassword());
        mailSender.setPort(gmailProperty.getPort());
        mailSender.setProtocol(gmailProperty.getProtocol());
        mailSender.setUsername(gmailProperty.getUsername());
        
        Properties mailSenderProperties = new Properties();
        mailSenderProperties.put("mail.smtp.starttls.enable", gmailProperty.getSmtp().getStarttlsEnable());
        mailSenderProperties.put("mail.smtp.auth", gmailProperty.getSmtp().getAuth());
        mailSenderProperties.put("mail.transport.protocol", gmailProperty.getProtocol());
        mailSenderProperties.put("mail.debug",gmailProperty.getDebug());
        
        mailSender.setJavaMailProperties(mailSenderProperties);
        return mailSender;
    }
    
    @Bean
    @Lazy
    @MailType(MailEnum.HOTMAIL)
    public JavaMailSender getHotmailJavaMailSender(){
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        return mailSender;
    }
}
