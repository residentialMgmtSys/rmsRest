package org.javasoft.rmsRest.entity.group;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.group.Groups;
import org.javasoft.rmsRest.entity.settings.core.Menu;
import org.javasoft.rmsRest.entity.settings.core.Module;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Groups.class)
public abstract class Groups_ {

	public static volatile SingularAttribute<Groups, Long> groupsId;
	public static volatile SingularAttribute<Groups, String> groupName;
	public static volatile SingularAttribute<Groups, String> subMenu;
	public static volatile SingularAttribute<Groups, String> flag;
	public static volatile SingularAttribute<Groups, Boolean> defaultGrp;
	public static volatile SingularAttribute<Groups, Module> module;
	public static volatile SingularAttribute<Groups, Menu> menu;
	public static volatile SingularAttribute<Groups, String> groupCode;
	public static volatile SingularAttribute<Groups, Date> createDate;

}

