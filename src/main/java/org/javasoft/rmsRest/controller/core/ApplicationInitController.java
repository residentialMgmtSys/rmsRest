/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.controller.core;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import static org.javasoft.rmsLib.api.util.InitDTOPath.START_UP_API;
import static org.javasoft.rmsLib.api.util.InitDTOPath.V1_API;
import org.javasoft.rmsLib.dto.util.InitDTO;
import org.javasoft.rmsRest.service.facility.EstateUnitService;
import org.javasoft.rmsRest.service.occupants.ResidentService;
import org.javasoft.rmsRest.service.settings.core.ModuleService;
import org.javasoft.rmsRest.service.settings.core.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ayojava
 */
@Slf4j
//@CrossOrigin
@RestController
@RequestMapping(V1_API)
@Api(value="ApplicationInitController", description = "Basic start up values for rmsLib")
public class ApplicationInitController {
    
    @Autowired
    private EstateUnitService estateUnitService;
    
    @Autowired
    private ResidentService residentService;
    
    @Autowired
    private ProfileService profileService;
    
    @Autowired
    private ModuleService moduleService;
    
    @GetMapping(START_UP_API)
    @ApiOperation(value = "Return Start Up Values", response = InitDTO.class)
    public InitDTO startUp(){
        InitDTO initDTO = new InitDTO();
        initDTO.setEstateUnitDTOs(estateUnitService.findAll());
        initDTO.setResidentDTOs(residentService.findAll());
        initDTO.setModuleDTOs(moduleService.findAll());
        initDTO.setProfileDTOs(profileService.findAll());
        return initDTO;
    }
}
