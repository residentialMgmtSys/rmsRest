package org.javasoft.rmsRest.entity.settings.core;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.settings.core.Menu;
import org.javasoft.rmsRest.entity.settings.core.Module;
import org.javasoft.rmsRest.entity.settings.core.Profile;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Module.class)
public abstract class Module_ {

	public static volatile SingularAttribute<Module, String> moduleCode;
	public static volatile SingularAttribute<Module, String> flag;
	public static volatile SingularAttribute<Module, Profile> profile;
	public static volatile SingularAttribute<Module, String> moduleName;
	public static volatile ListAttribute<Module, Menu> menus;
	public static volatile SingularAttribute<Module, Long> moduleId;
	public static volatile SingularAttribute<Module, Date> createDate;

}

