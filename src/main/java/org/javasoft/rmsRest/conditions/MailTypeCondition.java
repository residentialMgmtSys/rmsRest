/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.conditions;

import java.util.Map;
import static org.javasoft.rmsLib.utils.constants.RmsConstants.DEFAULT_MAIL_TYPE;
import org.javasoft.rmsRest.qualifiers.MailEnum;
import org.javasoft.rmsRest.qualifiers.MailType;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 *
 * @author ayojava
 */
public class MailTypeCondition implements Condition{

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Map<String,Object> attributes = metadata.getAnnotationAttributes(MailType.class.getName());
        //String type = (String) attributes.getOrDefault("value","");
        String type = ((MailEnum)attributes.get("value")).name();
        String defaultType = System.getProperty("mailType", DEFAULT_MAIL_TYPE);
        return defaultType.equalsIgnoreCase(type);
    }
    
}
