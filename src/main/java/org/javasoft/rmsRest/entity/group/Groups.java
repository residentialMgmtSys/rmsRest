/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.group;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;
import org.javasoft.rmsRest.entity.settings.core.Menu;
import org.javasoft.rmsRest.entity.settings.core.Module;

/**
 *
 * @author ayojava
 */
@Data
@MappedSuperclass
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public abstract class  Groups  implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long groupsId;

    @NotEmpty
    private String groupName;
    
    @NotEmpty
    private String groupCode;
    
    @NotEmpty
    private String flag;
    
    private boolean defaultGrp;//this indicates their is no need to display in the front end
    
    private String subMenu;//use the discriminatorItn for 
    
    @ManyToOne
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name ="moduleId",nullable = false)
    private Module module;
    
    @ManyToOne
    @JoinColumn(name ="menuId",nullable = false)
    private Menu menu;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @CreationTimestamp
    private Date createDate;
    
}
