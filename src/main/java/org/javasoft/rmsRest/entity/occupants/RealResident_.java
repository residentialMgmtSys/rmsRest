package org.javasoft.rmsRest.entity.occupants;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.facility.ResidentialUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RealResident.class)
public abstract class RealResident_ extends org.javasoft.rmsRest.entity.occupants.Resident_ {

	public static volatile SingularAttribute<RealResident, ResidentialUnit> residentialUnit;

}

