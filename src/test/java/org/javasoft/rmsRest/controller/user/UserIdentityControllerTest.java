/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.controller.user;

import org.hamcrest.CoreMatchers;
import static org.javasoft.rmsLib.api.user.UserIdentityDTOPath.LOGIN_API;
import static org.javasoft.rmsLib.api.user.UserIdentityDTOPath.V1_API;
import org.javasoft.rmsLib.dto.user.UserIdentityDTO;
import org.javasoft.rmsLib.utils.PasswordUtil;
import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author ayojava
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@TestPropertySource(locations = "classpath:test.properties")
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserIdentityControllerTest {
    
//    @LocalServerPort
    private int port;

    private String defaultURL , fullPath;

//    @Autowired
    private TestRestTemplate restTemplate;
    
    private UserIdentityDTO userIdentity;
    
//    @Autowired
    private PasswordUtil passwordUtil;
    
//    @Before
    public void before() {
        defaultURL = "http://localhost:" + this.port;
        fullPath = defaultURL + V1_API + LOGIN_API;    
    }
    
//    @Test
    public void testUserIdentityDTO(){
        String userName ="SYS01";
        String password = "admin";
        userIdentity = restTemplate.getForObject(fullPath, UserIdentityDTO.class,userName,password);
        assertNotNull(" No user was obtained with the credentials ",userIdentity);
        Assert.assertNotNull("Username is null ", userIdentity.getUserName());
        Assert.assertThat("Username is different", "SYS01", CoreMatchers.is(userIdentity.getUserName()));
        Assert.assertThat("PasswordHash is different", passwordUtil.encrypt("ADMIN"), CoreMatchers.is(userIdentity.getUserName()));
    }
    
//    @Test
    public void testUserIdentityDTODetails(){
        
    }
}
