package org.javasoft.rmsRest.controller.core;

import static org.javasoft.rmsLib.api.util.InitDTOPath.START_UP_API;
import static org.javasoft.rmsLib.api.util.InitDTOPath.V1_API;
import org.javasoft.rmsLib.dto.util.InitDTO;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import static org.springframework.util.Assert.notEmpty;

//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@TestPropertySource(locations = "classpath:test.properties")
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApplicationInitControllerTest {

//    @LocalServerPort
    private int port;

    private String defaultURL , fullPath;

//    @Autowired
    private TestRestTemplate restTemplate;
    
    private InitDTO initDTO;

//    @Before
    public void before() {
        defaultURL = "http://localhost:" + this.port;
        fullPath = defaultURL + V1_API + START_UP_API;
        initDTO = restTemplate.getForObject(fullPath, InitDTO.class);
    }

//   @Test
    public void testInitDTO(){
        assertNotNull(initDTO);
        assertTrue("There is no data available ", initDTO.isDataAvailable());
    }
    
//    @Test
    public void testEstateUnitDTOs(){
        notEmpty(initDTO.getEstateUnitDTOs(), "EstateUnitDTO list is null");
    }
    
//    @Test
    public void testModuleDTOs(){
        notEmpty(initDTO.getModuleDTOs(), "ModuleDTO list is null");
    }
    
//    @Test
    public void testProfileDTOs(){
        notEmpty(initDTO.getProfileDTOs(), "ProfileDTO list is null");
    }
    
//    @Test
    public void testResidentDTOs(){
        notEmpty(initDTO.getResidentDTOs(), "ResidentDTO list is null");
    }
}
