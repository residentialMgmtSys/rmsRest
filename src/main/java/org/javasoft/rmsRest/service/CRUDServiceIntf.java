/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.service;

import java.util.List;

/**
 *
 * @author ayojava
 * @param <E>
 */
public interface CRUDServiceIntf<E> {

    E getById(Long id);

    List<E> findAll();

    void delete(Long id);
}
