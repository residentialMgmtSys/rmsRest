/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.account;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;
import org.javasoft.rmsRest.entity.flow.EntityState;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@Table(name = "AccountTemplate")
@NoArgsConstructor
public class AccountTemplate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long accountTemplateId;
    
    @Column(unique = true,name="accountType")
    private String accountType ; // GH
    
    @NotEmpty
    @Column(name="txnFlow")
    private String txnFlow ; //DR , CR , BOTH ,NONE
    
    @Column(name="overdraftAllowed")
    @org.hibernate.annotations.Type(type = "yes_no")
    private boolean overdraftAllowed;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false,name="createDate")
    @CreationTimestamp
    private Date createDate;
    
    @JoinColumn(name = "entityStateId", referencedColumnName = "entityStateId", nullable = false)
    @OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @Fetch(value = FetchMode.SELECT)
    private EntityState entityState; // 
    
}
