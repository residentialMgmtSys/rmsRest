/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.mail;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.occupants.Resident;

/**
 *
 * @author ayojava
 */
@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EmailRecipients.class)
public class EmailRecipients_ {
    
    public static volatile SingularAttribute<EmailRecipients, Long> emailRecipientsId;
    public static volatile SingularAttribute<EmailRecipients, Resident> recipient;
    public static volatile SingularAttribute<EmailRecipients, Email> email;
    public static volatile SingularAttribute<EmailRecipients, String> flag;
    public static volatile SingularAttribute<EmailRecipients, String> recipentType;
    public static volatile SingularAttribute<EmailRecipients, Date> createDate;
}
