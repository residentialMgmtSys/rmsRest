/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.exception;

import static java.util.stream.Collectors.joining;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import static org.javasoft.rmsLib.utils.error.ErrorCodes.E003;
import static org.javasoft.rmsLib.utils.error.ErrorCodes.E004;
import static org.javasoft.rmsLib.utils.error.ErrorCodes.E005;
import org.javasoft.rmsLib.utils.error.ErrorDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 *
 * @author ayojava
 */
@Slf4j
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GenericExceptionHandler {

    @Autowired
    private ErrorBuilder errorBuilder;
    
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorDetail> handleResourceNotFound(ResourceNotFoundException exceptionObj,HttpServletRequest requestObj){    
        errorBuilder.setErrorDetail(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name(), exceptionObj.getMessage(), 
                exceptionObj.getClass().getName());
        return new ResponseEntity<>(errorBuilder.getErrorDetail(),HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(LoginException.class)
    public ResponseEntity<ErrorDetail> handleLoginException(LoginException exceptionObj,HttpServletRequest requestObj){    
        errorBuilder.setErrorDetail(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), exceptionObj.getMessage(), 
                exceptionObj.getClass().getName());
        return new ResponseEntity<>(errorBuilder.getErrorDetail(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorDetail> handleDataIntegrityViolationException(DataIntegrityViolationException ex,WebRequest request) {  
        errorBuilder.setErrorDetail(HttpStatus.CONFLICT.value(), HttpStatus.CONFLICT.name(), E003, ex.getClass().getName());
        log.error("Exception :: ", ex);
        return new ResponseEntity<>(errorBuilder.getErrorDetail(),HttpStatus.CONFLICT);
    }
    
    /* Handles ConstraintViolationException. Thrown when @Validated fails. */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorDetail> handleConstraintViolationException(ConstraintViolationException ex) {
        
        errorBuilder.setErrorDetail(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(), E004,ex.getClass().getName());
        errorBuilder.addConstraintViolationErrors(ex.getConstraintViolations());
        log.error("Exception :: ", ex);
        return new ResponseEntity<>(errorBuilder.getErrorDetail(),HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDetail> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        errorBuilder.setErrorDetail(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(), E005,ex.getClass().getName());
        errorBuilder.addFieldValidationErrors(ex.getBindingResult().getFieldErrors());
        log.error("Exception :: ", ex);
        return new ResponseEntity<>(errorBuilder.getErrorDetail(),HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<ErrorDetail> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException ex) {
        StringBuilder message = new StringBuilder();
        message.append(ex.getContentType()).append(" media type is not supported. Supported media types are ").
                append(ex.getSupportedMediaTypes().stream().map(MediaType::getType).collect(joining(",")));
        errorBuilder.setErrorDetail(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), HttpStatus.UNSUPPORTED_MEDIA_TYPE.name(), message.toString(), 
                ex.getClass().getName());
        log.error("Exception :: ", ex);
        return new ResponseEntity<>(errorBuilder.getErrorDetail(),HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }
    
}
