/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.account;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author ayojava
 */
@Data
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
public abstract class AbstractAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "accountID_GEN", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "accountID_GEN", sequenceName = "accountID_SEQ")
    private Long accountId;

    @NotEmpty
    @Column(unique = true,name="accountNo")
    private String accountNo;

    @NotEmpty
    @Column(name="accountName")
    private String accountName;

    @NotEmpty
    @Column(unique = true, name="systemAccountNo")
    private String systemAccountNo; // system generated which is unique and will be unchanged 
        
    @NotEmpty
    private String flag ; // Active or Locked or Suspended 
    
    @ColumnDefault(value = "0.0")
    @Column(name="accountBalance")
    private Double accountBalance ;
    
    @JoinColumn(name = "accountTemplateId",nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private AccountTemplate accountTemplate;
    
    @NotEmpty
    @Column(name="balStatus")
    private String balStatus ; //credit /debit/ zero z
        
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false,name="createDate")
    @CreationTimestamp
    private Date createDate;

}
