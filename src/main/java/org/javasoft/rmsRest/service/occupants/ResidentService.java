/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.service.occupants;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rmsLib.dto.occupants.ResidentDTO;
import org.javasoft.rmsRest.mapper.occupants.ResidentMapper;
import org.javasoft.rmsRest.repository.occupants.ResidentRepository;
import org.javasoft.rmsRest.service.CRUDServiceIntf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ayojava
 */
@Slf4j
@Service
@Transactional
public class ResidentService implements CRUDServiceIntf<ResidentDTO>{
    
    private final ResidentRepository residentRepository;
    
    @Autowired
    private ResidentMapper residentMapper;
    
    @Autowired
    public ResidentService(ResidentRepository residentRepository){
        this.residentRepository = residentRepository;
    }
    
    @Override
    public List<ResidentDTO> findAll(){
        return residentMapper.convertEntityListToDTOList(residentRepository.findAllBy());
    }

    @Override
    public ResidentDTO getById(Long id) {
        return residentMapper.convertEntityToDTO(residentRepository.getOne(id));
    }

    @Override
    public void delete(Long id) {
        residentRepository.deleteById(id);
    }
}
