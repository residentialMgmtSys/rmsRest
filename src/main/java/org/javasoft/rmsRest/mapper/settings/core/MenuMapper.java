/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper.settings.core;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.javasoft.rmsLib.dto.settings.core.FunctionsDTO;
import org.javasoft.rmsLib.dto.settings.core.MenuDTO;
import org.javasoft.rmsRest.entity.settings.core.Menu;
import org.javasoft.rmsRest.mapper.GenericMapper;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Component
public class MenuMapper implements GenericMapper<MenuDTO,Menu>{

    @Override
    public List<MenuDTO> convertEntityListToDTOList(Stream<Menu> entities) {
        return entities.map(convertEntityToDTO).collect(Collectors.toList());
    }
    
    @Override
    public MenuDTO convertEntityToDTO(Menu entity){
        return convertEntityToDTO.apply(entity);
    }
    
    private Function<Menu, MenuDTO> convertEntityToDTO=(Menu entity) -> {
            FunctionsDTO functionDTO = new FunctionsDTO(entity.getFunctions().isApprovalFn(),entity.getFunctions().isDeleteFn(),entity.getFunctions().isEditFn(),
                    entity.getFunctions().isListFn(),entity.getFunctions().isNewFn(),entity.getFunctions().isViewFn());
                return new MenuDTO(entity.getMenuId(), entity.getMenuCode(), entity.getMenuName(), entity.getFlag(),functionDTO);
        };
}
