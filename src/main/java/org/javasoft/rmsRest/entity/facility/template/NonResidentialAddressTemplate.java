/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.facility.template;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author ayojava
 */
@Data
@NoArgsConstructor
@Embeddable
public class NonResidentialAddressTemplate implements Serializable {
    
    @NotBlank
    @Column(name="unitNo")
    private int unitNo;
    
    @NotBlank
    @Column(name="identificationNo")
    private String identificationNo;
    
    @NotBlank
    @Column(name="zoneCode")
    private String zoneCode;
    
    @NotBlank
    @Column(name="otherAddress")
    private String otherAddress ;
}
