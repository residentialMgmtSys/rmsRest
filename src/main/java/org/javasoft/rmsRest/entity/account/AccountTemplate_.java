package org.javasoft.rmsRest.entity.account;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.account.AccountTemplate;
import org.javasoft.rmsRest.entity.flow.EntityState;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AccountTemplate.class)
public abstract class AccountTemplate_ {

	public static volatile SingularAttribute<AccountTemplate, Boolean> overdraftAllowed;
	public static volatile SingularAttribute<AccountTemplate, EntityState> entityState;
	public static volatile SingularAttribute<AccountTemplate, String> accountType;
	public static volatile SingularAttribute<AccountTemplate, Long> accountTemplateId;
	public static volatile SingularAttribute<AccountTemplate, String> txnFlow;
	public static volatile SingularAttribute<AccountTemplate, Date> createDate;

}

