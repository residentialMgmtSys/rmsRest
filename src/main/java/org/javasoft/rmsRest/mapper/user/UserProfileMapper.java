/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper.user;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.javasoft.rmsLib.dto.settings.core.MenuDTO;
import org.javasoft.rmsLib.dto.settings.core.ModuleDTO;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;
import org.javasoft.rmsLib.dto.user.UserProfileDTO;
import org.javasoft.rmsRest.entity.settings.core.Menu;
import org.javasoft.rmsRest.entity.settings.core.Module;
import org.javasoft.rmsRest.entity.settings.core.Profile;
import org.javasoft.rmsRest.entity.user.UserProfile;
import org.javasoft.rmsRest.mapper.GenericMapper;
import org.javasoft.rmsRest.mapper.settings.core.MenuMapper;
import org.javasoft.rmsRest.mapper.settings.core.ModuleMapper;
import org.javasoft.rmsRest.mapper.settings.core.ProfileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Component
public class UserProfileMapper implements GenericMapper<UserProfileDTO,UserProfile>{

    @Autowired
    ProfileMapper profileMapper;

    @Autowired
    ModuleMapper moduleMapper;

    @Autowired
    MenuMapper menuMapper;

    @Override
    public List<UserProfileDTO> convertEntityListToDTOList(Stream<UserProfile> entities) {
        return entities.map(convertEntityToDTO).collect(Collectors.toList());
    }

    @Override
    public UserProfileDTO convertEntityToDTO(UserProfile entity) {
        return convertEntityToDTO.apply(entity);
    }

    private Function<UserProfile, UserProfileDTO> convertEntityToDTO=(UserProfile entity) -> {
            UserProfileDTO userProfileDTO = new UserProfileDTO();
            userProfileDTO.setCreateDate(entity.getCreateDate());
            userProfileDTO.setUserProfileId(entity.getUserProfileId());

            Optional<Menu> menuOptional = Optional.ofNullable(entity.getMenu());
            Optional<Module> moduleOptional = Optional.ofNullable(entity.getModule());
            Optional<Profile> profileOptional = Optional.ofNullable(entity.getProfile());

            MenuDTO menuDTO = menuOptional.flatMap((Menu menu) -> {
                return Optional.of(menuMapper.convertEntityToDTO(menu));
            }).orElse(new MenuDTO(false));

            ModuleDTO moduleDTO = moduleOptional.flatMap((Module module) -> {
                return Optional.of(moduleMapper.convertEntityToDTO(module));
            }).orElse(new ModuleDTO(false));

            ProfileDTO profileDTO = profileOptional.flatMap((Profile profile) -> {
                return Optional.of(profileMapper.convertEntityToDTO(profile));
            }).orElse(new ProfileDTO(false));

            userProfileDTO.setMenu(menuDTO);
            userProfileDTO.setModule(moduleDTO);
            userProfileDTO.setProfile(profileDTO);
            return userProfileDTO;
        };
    
}
