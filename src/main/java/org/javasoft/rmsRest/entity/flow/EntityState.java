/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.flow;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.javasoft.rmsRest.entity.account.AccountTemplate;
import org.javasoft.rmsRest.entity.facility.EstateUnit;
import org.javasoft.rmsRest.entity.occupants.Resident;
import org.javasoft.rmsRest.entity.user.UserIdentity;
import org.javasoft.rmsLib.utils.status.entityState.AbstractEntityStateImpl;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "EntityState")
@EqualsAndHashCode(of = {"entityStateId"},callSuper = false)
public class EntityState extends AbstractEntityStateImpl implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long entityStateId;
    
    @Column(nullable = false)
    private int status;
    
    @JoinColumn(name = "createdBy")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserIdentity createdBy;
    
    @JoinColumn(name = "lastModifiedBy")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserIdentity lastModifiedBy;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "entityState", fetch = FetchType.LAZY)
    private UserIdentity userIdentity;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "entityState", fetch = FetchType.LAZY)
    private EstateUnit estateUnit;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "entityState", fetch = FetchType.LAZY)
    private Resident resident;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "entityState", fetch = FetchType.LAZY)
    private AccountTemplate accountTemplate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false,name="createDate")
    @CreationTimestamp
    private Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date lastModifiedDate;
    
}
