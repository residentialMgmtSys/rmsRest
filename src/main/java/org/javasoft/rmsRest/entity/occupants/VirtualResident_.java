package org.javasoft.rmsRest.entity.occupants;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.facility.NonResidentialUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VirtualResident.class)
public abstract class VirtualResident_ extends org.javasoft.rmsRest.entity.occupants.Resident_ {

	public static volatile SingularAttribute<VirtualResident, NonResidentialUnit> nonResidentialUnit;
	public static volatile SingularAttribute<VirtualResident, String> contactAddress;

}

