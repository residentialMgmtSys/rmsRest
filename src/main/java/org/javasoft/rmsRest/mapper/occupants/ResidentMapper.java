/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper.occupants;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.javasoft.rmsLib.dto.occupants.ResidentDTO;
import org.javasoft.rmsRest.entity.occupants.Resident;
import org.javasoft.rmsRest.mapper.GenericMapper;
import org.javasoft.rmsRest.mapper.flow.EntityStateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Component
public class ResidentMapper implements GenericMapper<ResidentDTO,Resident>{
    
    @Autowired
    EntityStateMapper entityStateMapper;
    
    @Override
    public List<ResidentDTO> convertEntityListToDTOList(Stream<Resident> entities){
        return entities.map(convertEntityToDTO).collect(Collectors.toList());
    }
    
    @Override
    public ResidentDTO convertEntityToDTO(Resident entity){
        return convertEntityToDTO.apply(entity);
    }
    
    private Function<Resident, ResidentDTO> convertEntityToDTO=(Resident resident)->{
            ResidentDTO residentDTO = new ResidentDTO(resident.getResidentId(), resident.getTitle(), resident.getFirstName(), resident.getLastName(),
                    resident.getIdentificationNo(), resident.getEmailAddress(), resident.getAltEmailAddress(), resident.getPhoneNum(), 
                    resident.getAltPhoneNum(), resident.getOccupation(), resident.getSex(), resident.getReligion(), resident.getMaritalStatus(), 
                    resident.getResidentType(), resident.getEntryDate(), resident.getCreateDate(), resident.getBirthday(),
                    fullNameFunction().apply(resident));
            
            residentDTO.setEntityState(entityStateMapper.convertEntityToDTO(resident.getEntityState()));
            return residentDTO;
        };
    
    
     private  Function<Resident, String> fullNameFunction(){ return (Resident res) -> {
            StringBuilder builder = new StringBuilder();
            builder = builder.append(res.getTitle()).append(" ").append(res.getFirstName()).append(" ").append(res.getLastName());
            return builder.toString();
        };
     }
}
