/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.config;

import org.javasoft.rmsRest.exception.ErrorBuilder;
import org.javasoft.rmsLib.utils.PasswordUtil;
import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

/**
 *
 * @author ayojava
 */
@Configuration
public class BeanConfig {
    
    @Bean @Lazy
    @Scope(value = SCOPE_PROTOTYPE)
    public PasswordUtil passwordUtil(){
        return new PasswordUtil();
    }
    
    @Lazy
    @Bean(name = "errorBuilder") 
    @Scope(value = SCOPE_PROTOTYPE)
    public ErrorBuilder errorBuilder(){
        return new ErrorBuilder();
    }
}
