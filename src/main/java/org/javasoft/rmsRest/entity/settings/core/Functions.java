/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.settings.core;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author ayojava
 */

@Data
@Embeddable
@NoArgsConstructor
public class Functions implements Serializable {

    @Column(name="approvalFn")
    private boolean approvalFn;
    
    @Column(name="deleteFn")
    private boolean deleteFn;
    
    @Column(name="editFn")
    private boolean editFn;
    
    @Column(name="listFn")
    private boolean listFn;
    
    @Column(name="newFn")
    private boolean newFn;
    
    @Column(name="viewFn")
    private boolean viewFn;
    
    
    
}
