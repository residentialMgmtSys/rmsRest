/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.property;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 *
 * @author ayojava
 */

@Validated
@Component
@PropertySource("classpath:config/mail/gmailConfig.properties")
@ConfigurationProperties(prefix = "gmail", ignoreUnknownFields = false)
public class GMailProperty {

    @NotNull
    @Getter @Setter
    private String host;

    @Getter @Setter
    private int port;

    @NotNull
    @Getter @Setter
    private String username;

    @NotNull
    @Getter @Setter
    private String password;

    public static class Smtp {
        
        @Getter @Setter
        private String starttlsEnable;
        
        @Getter @Setter
        private String starttlsRequired;
        
        @Getter @Setter
        private String auth;

        @Getter @Setter
        private int connectionTimeout;   
        
    }

    @NotNull
    @Getter @Setter
    private String protocol;

    @Getter @Setter
    private String debug;
    
    @Getter @Setter
    private Smtp smtp;
}
