/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.repository.facility;

import org.javasoft.rmsRest.entity.facility.ResidentialUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ayojava
 */
@Repository
public interface ResidentialUnitRepository extends JpaRepository<ResidentialUnit,Long>{
    
}
