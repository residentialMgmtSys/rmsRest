/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.specification.facility;

import javax.persistence.criteria.Path;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.APPROVED;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.MODIFIED;
import org.javasoft.rmsRest.entity.facility.ResidentialUnit;
import org.javasoft.rmsRest.entity.facility.ResidentialUnit_;
import org.javasoft.rmsRest.entity.flow.EntityState_;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author ayojava
 */
public class ResidentialUnitSpecification {
    
    public static Specification<ResidentialUnit> findApprovedResidentialUnits(){
        return (root, query, cb) -> {
            Path<Integer> entityState = root.get(ResidentialUnit_.entityState).get(EntityState_.status); 
            return cb.isTrue(entityState.in(APPROVED,MODIFIED));
        };
    }
}
