package org.javasoft.rmsRest.entity.flow;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.account.AccountTemplate;
import org.javasoft.rmsRest.entity.facility.EstateUnit;
import org.javasoft.rmsRest.entity.occupants.Resident;
import org.javasoft.rmsRest.entity.user.UserIdentity;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EntityState.class)
public abstract class EntityState_ {

	public static volatile SingularAttribute<EntityState, AccountTemplate> accountTemplate;
	public static volatile SingularAttribute<EntityState, Long> entityStateId;
	public static volatile SingularAttribute<EntityState, UserIdentity> createdBy;
	public static volatile SingularAttribute<EntityState, Date> lastModifiedDate;
	public static volatile SingularAttribute<EntityState, EstateUnit> estateUnit;
	public static volatile SingularAttribute<EntityState, UserIdentity> lastModifiedBy;
	public static volatile SingularAttribute<EntityState, UserIdentity> userIdentity;
	public static volatile SingularAttribute<EntityState, Integer> status;
	public static volatile SingularAttribute<EntityState, Resident> resident;
	public static volatile SingularAttribute<EntityState, Date> createDate;

}

