package org.javasoft.rmsRest.entity.user;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.facility.EstateUnit;
import org.javasoft.rmsRest.entity.flow.EntityState;
import org.javasoft.rmsRest.entity.group.UserIdentityGroup;
import org.javasoft.rmsRest.entity.occupants.Resident;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserIdentity.class)
public abstract class UserIdentity_ {

	public static volatile SingularAttribute<UserIdentity, String> flag;
	public static volatile ListAttribute<UserIdentity, UserIdentityGroup> userIdentityGroups;
	public static volatile SingularAttribute<UserIdentity, Long> userIdentityId;
	public static volatile SingularAttribute<UserIdentity, String> userName;
	public static volatile SingularAttribute<UserIdentity, Resident> residentDetails;
	public static volatile SingularAttribute<UserIdentity, String> passwordHash;
	public static volatile ListAttribute<UserIdentity, UserProfile> systemUserProfile;
	public static volatile SingularAttribute<UserIdentity, EntityState> entityState;
	public static volatile SingularAttribute<UserIdentity, EstateUnit> estateUnit;
	public static volatile SingularAttribute<UserIdentity, String> systemNo;
	public static volatile SingularAttribute<UserIdentity, Boolean> sysAdmin;
	public static volatile SingularAttribute<UserIdentity, String> status;
	public static volatile SingularAttribute<UserIdentity, Date> createDate;

}

