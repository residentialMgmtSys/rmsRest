/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  ayojava
 * Created: 4 Oct, 2017
 */
INSERT INTO Profile (profileId , profileName , profileCode ,flag , createDate) VALUES (1,'Admin' ,'AD','AC',CURRENT_TIMESTAMP());
INSERT INTO Profile (profileId , profileName , profileCode ,flag , createDate) VALUES (2,'Resident' ,'RS','AC',CURRENT_TIMESTAMP());
INSERT INTO Profile (profileId , profileName , profileCode ,flag , createDate) VALUES (3,'Vendor' ,'VN','IN',CURRENT_TIMESTAMP());

INSERT INTO Modules (moduleId ,moduleCode,moduleName,flag,profileId,createDate) VALUES (1,'m_Administration','Administration','AC',1,CURRENT_TIMESTAMP());
INSERT INTO Modules (moduleId ,moduleCode,moduleName,flag,profileId,createDate) VALUES (2,'m_Facility','Facility','AC',1,CURRENT_TIMESTAMP());
INSERT INTO Modules (moduleId ,moduleCode,moduleName,flag,profileId,createDate) VALUES (3,'m_Residents','Residents','AC',1,CURRENT_TIMESTAMP());
INSERT INTO Modules (moduleId ,moduleCode,moduleName,flag,profileId,createDate) VALUES (4,'m_SystemUsers','System Users','AC',1,CURRENT_TIMESTAMP());

INSERT INTO Menu (menuId ,menuCode,menuName,flag,createDate,moduleId, approvalFn,deleteFn,editFn,listFn,newFn,viewFn) VALUES (1,'mn_Modules','Modules' ,'AC' ,CURRENT_TIMESTAMP(),1,0,0,0,0,0,0);
INSERT INTO Menu (menuId ,menuCode,menuName,flag,createDate,moduleId, approvalFn,deleteFn,editFn,listFn,newFn,viewFn) VALUES (2,'mn_Functions','Functions' ,'AC' ,CURRENT_TIMESTAMP(),1,0,0,0,0,0,0);
INSERT INTO Menu (menuId ,menuCode,menuName,flag,createDate,moduleId, approvalFn,deleteFn,editFn,listFn,newFn,viewFn) VALUES (3,'mn_Groups','Groups' ,'AC' ,CURRENT_TIMESTAMP(),2,0,0,0,0,0,0);
INSERT INTO Menu (menuId ,menuCode,menuName,flag,createDate,moduleId, approvalFn,deleteFn,editFn,listFn,newFn,viewFn) VALUES (4,'mn_Groups','Groups' ,'AC' ,CURRENT_TIMESTAMP(),3,0,0,0,0,0,0);
INSERT INTO Menu (menuId ,menuCode,menuName,flag,createDate,moduleId, approvalFn,deleteFn,editFn,listFn,newFn,viewFn) VALUES (5,'mn_Groups','Groups' ,'AC' ,CURRENT_TIMESTAMP(),4,0,0,0,0,0,0);

INSERT INTO EstateUnitGroup(groupsId,groupName,groupCode,flag,moduleId,menuId,createDate,defaultGrp,subMenu) VALUES (1,'Estate Units Group','EUG_1','AC',2,3,CURRENT_TIMESTAMP(),1,'EUG');
INSERT INTO EstateUnitGroup(groupsId,groupName,groupCode,flag,moduleId,menuId,createDate,defaultGrp,subMenu) VALUES (2,'Residential Units Group','RUG_2','AC',2,3,CURRENT_TIMESTAMP(),1,'RUG');
INSERT INTO EstateUnitGroup(groupsId,groupName,groupCode,flag,moduleId,menuId,createDate,defaultGrp,subMenu) VALUES (3,'Non Residential Units Group','NRUG_3','AC',2,3,CURRENT_TIMESTAMP(),1,'NRUG');

INSERT INTO ResidentGroup(groupsId,groupName,groupCode,flag,moduleId,menuId,createDate,defaultGrp,subMenu) VALUES (1,'Resident Groups','RG_1','AC',3,4,CURRENT_TIMESTAMP(),1,'RG');
INSERT INTO ResidentGroup(groupsId,groupName,groupCode,flag,moduleId,menuId,createDate,defaultGrp,subMenu) VALUES (2,'Real Residents Group','RRG_2','AC',3,4,CURRENT_TIMESTAMP(),1,'RRG');
INSERT INTO ResidentGroup(groupsId,groupName,groupCode,flag,moduleId,menuId,createDate,defaultGrp,subMenu) VALUES (3,'Virtual Residents Group','VRG_3','AC',3,4,CURRENT_TIMESTAMP(),1,'VRG');

INSERT INTO UserIdentityGroup(groupsId,groupName,groupCode,flag,moduleId,menuId,createDate,defaultGrp,subMenu) VALUES (1,'System Users Group','grp_Users','AC',4,5,CURRENT_TIMESTAMP(),1,'SUG');

INSERT INTO EntityState (entityStateId ,status, createDate ,lastModifiedDate,createdBy,lastModifiedBy)VALUES (1,1,CURRENT_TIMESTAMP(),null,null,null); 
INSERT INTO AccountTemplate(accountTemplateId,accountType,createDate,overdraftAllowed,txnFlow,entityStateId) VALUES (1,'GH',CURRENT_TIMESTAMP(),'Y','NONE',1);
INSERT INTO EstateUnitAccount (accountId,accountBalance,accountName,accountNo,balStatus,createDate,flag,systemAccountNo,accountTemplateId) VALUES (1,0.0,'GATE HOUSE','GH001','Z',CURRENT_TIMESTAMP(),'AC','GHOO1',1);

INSERT INTO EntityState (entityStateId ,status, createDate ,lastModifiedDate,createdBy,lastModifiedBy)VALUES (2,1,CURRENT_TIMESTAMP(),null,null,null); 
INSERT INTO EstateUnit(estateUnitId,actualEstateUnitId,createDate,status,unitSequence,unitType,entityStateId,estateUnitAccountId) VALUES (1,1,CURRENT_TIMESTAMP(),'OC',0,'NRU',2,1);
INSERT INTO NonResidentialUnit(activity,identificationNo,otherAddress,unitNo,zoneCode,nonResidentialType,estateUnitId)VALUES ('OF','OF001','GATE HOUSE',1,'G','GH',1);

INSERT INTO EstateUnitGroup_EstateUnit (groupsId,estateUnitId) VALUES (1,1);
INSERT INTO EstateUnitGroup_EstateUnit (groupsId,estateUnitId) VALUES (3,1);

INSERT INTO EntityState (entityStateId ,status, createDate ,lastModifiedDate,createdBy,lastModifiedBy)VALUES (3,1,CURRENT_TIMESTAMP(),null,null,null); 
INSERT INTO AccountTemplate(accountTemplateId,accountType,createDate,overdraftAllowed,txnFlow,entityStateId) VALUES (2,'VRA',CURRENT_TIMESTAMP(),'Y','BOTH',3);
INSERT INTO ResidentAccount(accountId,accountBalance,accountName,accountNo,balStatus,createDate,flag,systemAccountNo,accountTemplateId) VALUES (1,0.0,'ADMIN','VR001','Z',CURRENT_TIMESTAMP(),'AC','VR0001',2);

INSERT INTO EntityState (entityStateId ,status, createDate ,lastModifiedDate,createdBy,lastModifiedBy)VALUES (4,1,CURRENT_TIMESTAMP(),null,null,null); 
INSERT INTO Resident(residentId,altEmailAddress,altPhoneNum,birthday,createDate,emailAddress,entryDate,firstName,identificationNo,lastName,maritalStatus,occupation,phoneNum,religion,residentType,sex,title,entityStateId,residentAccountId) VALUES(1,'a@a.com','000',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'a@a.com',CURRENT_TIMESTAMP(),'admin','A0001','ADMIN','S','ADMIN','00000','OTHER','VR','M','M',4,1);
INSERT INTO VirtualResident(contactAddress,residentId) VALUES ('GATE HOUSE',1);
INSERT INTO VirtualResident_NonResidentialUnit (nonResidentialUnitId,residentId) VALUES (1,1);

INSERT INTO ResidentGroup_Resident (groupsId,residentId) VALUES (1,1);
INSERT INTO ResidentGroup_Resident (groupsId,residentId) VALUES (3,1);

INSERT INTO EntityState (entityStateId ,status, createDate ,lastModifiedDate,createdBy,lastModifiedBy)VALUES (5,1,CURRENT_TIMESTAMP(),null,null,null); 
INSERT INTO UserIdentity(userIdentityId,createDate,flag,passwordHash,status,sysAdmin,systemNo,userName,entityStateId,residentId) VALUES (1,CURRENT_TIMESTAMP(),'AC','1jV7PGBi7lc6y49cYKa306V2oUHxuszH','LO',1,'SYS01','SYS01',5,1);
INSERT INTO UserIdentity_EstateUnit (estateUnitId,userIdentityId) VALUES (1,1);
INSERT INTO UserIdentityGroup_UserIdentity(groupsId,userIdentityId) VALUES (1,1);

INSERT INTO ResidentAccount(accountId,accountBalance,accountName,accountNo,balStatus,createDate,flag,systemAccountNo,accountTemplateId) VALUES (2,0.0,'ADMIN','VR002','Z',CURRENT_TIMESTAMP(),'AC','VR0002',2);

INSERT INTO EntityState (entityStateId ,status, createDate ,lastModifiedDate,createdBy,lastModifiedBy)VALUES (6,1,CURRENT_TIMESTAMP(),null,null,null); 
INSERT INTO Resident(residentId,altEmailAddress,altPhoneNum,birthday,createDate,emailAddress,entryDate,firstName,identificationNo,lastName,maritalStatus,occupation,phoneNum,religion,residentType,sex,title,entityStateId,residentAccountId) VALUES(2,'a@a.com','300',CURRENT_TIMESTAMP(),CURRENT_TIMESTAMP(),'a@a.com',CURRENT_TIMESTAMP(),'admin','A0002','ADMIN','S','ADMIN','00000','OTHER','VR','M','M',6,2);
INSERT INTO VirtualResident(contactAddress,residentId) VALUES ('GATE HOUSE',2);
INSERT INTO VirtualResident_NonResidentialUnit (nonResidentialUnitId,residentId) VALUES (1,2);

INSERT INTO ResidentGroup_Resident (groupsId,residentId) VALUES (1,2);
INSERT INTO ResidentGroup_Resident (groupsId,residentId) VALUES (3,2);


INSERT INTO EntityState (entityStateId ,status, createDate ,lastModifiedDate,createdBy,lastModifiedBy)VALUES (7,1,CURRENT_TIMESTAMP(),null,null,null); 
INSERT INTO UserIdentity(userIdentityId,createDate,flag,passwordHash,status,sysAdmin,systemNo,userName,entityStateId,residentId) VALUES (2,CURRENT_TIMESTAMP(),'AC','1jV7PGBi7lc6y49cYKa306V2oUHxuszH','LO',1,'SYS02','SYS02',7,2);
INSERT INTO UserIdentity_EstateUnit (estateUnitId,userIdentityId) VALUES (1,2);
INSERT INTO UserIdentityGroup_UserIdentity(groupsId,userIdentityId) VALUES (1,2);


