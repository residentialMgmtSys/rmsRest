/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.mail;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.occupants.Resident;

/**
 *
 * @author ayojava
 */
@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Email.class)
public abstract class Email_ {
    
    public static volatile SingularAttribute<Email, Long> emailId;
    public static volatile SingularAttribute<Email, String> mailSubject;
    public static volatile SingularAttribute<Email, Boolean> attachment;
    public static volatile SingularAttribute<Email, Boolean> draft;
    public static volatile SingularAttribute<Email, String> attachmentFile;
    public static volatile SingularAttribute<Email, String> priorityType;
    public static volatile SingularAttribute<Email, String> status;
    public static volatile SingularAttribute<Email, String> mailMessage;
    public static volatile SingularAttribute<Email, Resident> sender;
    public static volatile ListAttribute<Email, EmailRecipients> emailRecipients;
    public static volatile SingularAttribute<Email, Date> createDate;
    
}
