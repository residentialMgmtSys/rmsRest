/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.service.user;

import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rmsRest.entity.user.UserIdentity;
import org.javasoft.rmsRest.mapper.user.UserIdentityMapper;
import org.javasoft.rmsRest.repository.user.UserIdentityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.javasoft.rmsLib.dto.user.UserIdentityDTO;
import org.javasoft.rmsRest.exception.ResourceNotFoundException;
import static org.javasoft.rmsRest.specification.user.UserIdentitySpecification.findByUsernameAndPasswordHash;
import org.javasoft.rmsLib.utils.PasswordUtil;
import static org.javasoft.rmsLib.utils.error.ErrorCodes.E031;
import org.javasoft.rmsRest.service.CRUDServiceIntf;
import static org.javasoft.rmsRest.specification.user.UserIdentitySpecification.findApprovedEntity;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author ayojava
 */
@Slf4j
@Service
@Transactional
public class UserIdentityService implements CRUDServiceIntf<UserIdentityDTO>{

    private final UserIdentityRepository userIdentityRepository;
    
    @Autowired
    private PasswordUtil passwordUtil;
    
    @Autowired
    private UserIdentityMapper userIdentityMapper;

    @Autowired
    public UserIdentityService(UserIdentityRepository userIdentityRepository) {
        this.userIdentityRepository = userIdentityRepository;
    }
    
    @Override
    public List<UserIdentityDTO> findAll(){
        userIdentityMapper.eagerFetch=false;
        return userIdentityMapper.convertEntityListToDTOList(userIdentityRepository.findAll(findApprovedEntity()).stream());
    }
       
    public UserIdentityDTO login(String userName, String password){
        
        Specification<UserIdentity> searchSpec= findByUsernameAndPasswordHash(userName.toUpperCase(),passwordUtil.encrypt(password.toUpperCase()));
        Optional<UserIdentity> userIdentityOpt = userIdentityRepository.findOne(searchSpec);
        
        UserIdentity userIdentity = userIdentityOpt.orElseThrow(() -> new ResourceNotFoundException(E031));
        return userIdentityMapper.convertEntityToDTO(userIdentity);      
    }

    @Override
    public UserIdentityDTO getById(Long id) {
        return userIdentityMapper.convertEntityToDTO(userIdentityRepository.getOne(id));
    }

    @Override
    public void delete(Long id) {
        userIdentityRepository.deleteById(id);
    }
     
    
}
