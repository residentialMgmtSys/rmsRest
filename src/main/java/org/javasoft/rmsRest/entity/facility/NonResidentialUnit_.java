package org.javasoft.rmsRest.entity.facility;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.facility.template.NonResidentialAddressTemplate;
import org.javasoft.rmsRest.entity.occupants.VirtualResident;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(NonResidentialUnit.class)
public abstract class NonResidentialUnit_ extends org.javasoft.rmsRest.entity.facility.EstateUnit_ {

	public static volatile SingularAttribute<NonResidentialUnit, String> nonResidentialType;
	public static volatile SingularAttribute<NonResidentialUnit, String> activity;
	public static volatile ListAttribute<NonResidentialUnit, VirtualResident> virtualResidents;
	public static volatile SingularAttribute<NonResidentialUnit, NonResidentialAddressTemplate> nonResidentialAddressTemplate;

}

