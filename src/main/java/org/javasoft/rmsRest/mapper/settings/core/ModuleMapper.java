/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper.settings.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.javasoft.rmsLib.dto.settings.core.MenuDTO;
import org.javasoft.rmsLib.dto.settings.core.ModuleDTO;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;
import org.javasoft.rmsRest.entity.settings.core.Menu;
import org.javasoft.rmsRest.entity.settings.core.Module;
import org.javasoft.rmsRest.entity.settings.core.Profile;
import org.javasoft.rmsRest.mapper.GenericMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Component
public class ModuleMapper implements GenericMapper<ModuleDTO, Module> {

    @Autowired
    private ProfileMapper profileMapper;

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<ModuleDTO> convertEntityListToDTOList(Stream<Module> entities) {
        return entities.map(convertEntityToDTO).collect(Collectors.toList());
    }

    @Override
    public ModuleDTO convertEntityToDTO(Module module) {
        return convertEntityToDTO.apply(module);
    }

    private Function<Module, ModuleDTO> convertEntityToDTO = (Module entity) -> {
        Optional<Profile> optionalProfile = Optional.ofNullable(entity.getProfile());
        ProfileDTO profileDTO = optionalProfile.flatMap((Profile profile) -> {
            return Optional.of(profileMapper.convertEntityToDTO(profile));
        }).orElse(new ProfileDTO(false));

        List<Menu> menus = entity.getMenus();
        List<MenuDTO> menuDTOs = (menus == null || menus.isEmpty()) ? new ArrayList<>() : menuMapper.convertEntityListToDTOList(menus.stream());
        ModuleDTO moduleDTO = new ModuleDTO(entity.getModuleId(), entity.getModuleCode(), entity.getModuleName(), entity.getFlag(), entity.getCreateDate());
        moduleDTO.setProfileDTO(profileDTO);
        moduleDTO.setMenuDTOs(menuDTOs);
        return moduleDTO;
    };

    private Function<ModuleDTO, Module> convertDTOtoEntity = (ModuleDTO entity) -> {
        Module module = new Module();
        module.setFlag(entity.getFlag());
        module.setModuleCode(entity.getModuleCode());
        module.setModuleId(entity.getModuleId());
        module.setModuleName(entity.getModuleName());
        return module;
    };
}
