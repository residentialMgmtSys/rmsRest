package org.javasoft.rmsRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RmsRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(RmsRestApplication.class, args);
    }
    
}

/*

@SpringBootApplication
public class rmsRestApplication extends SpringBootServletInitializer{

    public static void main(String[] args) {
        SpringApplication.run(rmsRestApplication.class, args);
    }
    
    private static Class<rmsRestApplication> applicationClass = rmsRestApplication.class;
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }
}

*/