/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.occupants;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.javasoft.rmsRest.entity.account.ResidentAccount;
import org.javasoft.rmsRest.entity.flow.EntityState;
import org.javasoft.rmsRest.entity.group.ResidentGroup;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public class Resident implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long residentId;
    
    @NotEmpty
    private String title;

    @NotEmpty
    @Column(name="firstName")
    private String firstName;

    @NotEmpty
    @Column(name="lastName")
    private String lastName;

    @NotEmpty
    @Column(unique = true,name="identificationNo")
    private String identificationNo;

    @Email
    @Column(name="emailAddress")
    private String emailAddress;
    
    @Email
    @Column(name="altEmailAddress")
    private String altEmailAddress;
    
    @Column(name="phoneNum")
    private String phoneNum;
    
    @Column(name="altPhoneNum")
    private String altPhoneNum;
    
    private String occupation;

    @NotEmpty
    private String sex;

    @NotEmpty
    private String religion;

    @NotEmpty
    @Column(name="maritalStatus")
    private String maritalStatus;

    @NotEmpty
    @Column(name="residentType")
    private String residentType;
    
    @Past
    @Column(name="entryDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;//moved in the estate

    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false,name="createDate")
    @CreationTimestamp
    private Date createDate;

    @Past
    @Column(name="birthday")
    @Temporal(TemporalType.DATE)
    private Date birthday;

    @JoinColumn(name = "entityStateId", referencedColumnName = "entityStateId", nullable = false)
    @OneToOne(optional = false, cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.EAGER)
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    private EntityState entityState;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "residentAccountId", nullable = false)
    @Cascade({org.hibernate.annotations.CascadeType.PERSIST})
    private ResidentAccount residentAccount;
    
    @ManyToMany(mappedBy = "residents")
    private List<ResidentGroup> residentGroups;
    
    //    move implementation from residentUtil
       
}
