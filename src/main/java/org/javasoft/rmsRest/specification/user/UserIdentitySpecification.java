/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.specification.user;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import lombok.extern.slf4j.Slf4j;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.APPROVED;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.MODIFIED;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.NEW;
import static org.javasoft.rmsLib.utils.status.entityState.EntityStateIntf.NEWLY_MODIFIED;
import org.javasoft.rmsRest.entity.flow.EntityState_;
import org.javasoft.rmsRest.entity.user.UserIdentity_;
import org.javasoft.rmsRest.entity.user.UserIdentity;

import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author ayojava
 */
@Slf4j
public class UserIdentitySpecification {
    
    public static Specification<UserIdentity> findByUsernameAndPasswordHash(String userName, String passwordHash){
        return (root, query, cb) -> {
               
            Path<String> userNamePath = root.<String>get(UserIdentity_.userName);
            Path<String> passwordHashPath = root.<String>get(UserIdentity_.passwordHash);
            
            Predicate userNamePredicate =cb.equal(userNamePath, userName);          
            Predicate passwordHashPredicate =cb.equal(passwordHashPath, passwordHash);
            
           return cb.and(userNamePredicate, passwordHashPredicate);
        };
    }
    
    public static Specification<UserIdentity> findApprovedEntity(){
        return (root, query, cb) -> {
            Path<Integer> entityState = root.get(UserIdentity_.entityState).get(EntityState_.status); 
            return cb.isTrue(entityState.in(APPROVED,MODIFIED));
        };
    }
    
    public static Specification<UserIdentity> findPendingEntity(){
        return (root, query, cb) -> {
            Path<Integer> entityState = root.get(UserIdentity_.entityState).get(EntityState_.status); 
            return cb.isTrue(entityState.in(NEW,NEWLY_MODIFIED));
        };
    }
}
