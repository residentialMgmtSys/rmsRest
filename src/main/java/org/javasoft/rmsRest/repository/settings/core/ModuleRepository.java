/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.repository.settings.core;

import java.util.stream.Stream;
import org.javasoft.rmsRest.entity.settings.core.Module;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ayojava
 */
@Repository
public interface ModuleRepository extends JpaRepository<Module,Long>{
    
    Stream<Module> findAllBy();
}
