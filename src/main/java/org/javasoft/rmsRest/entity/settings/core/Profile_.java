package org.javasoft.rmsRest.entity.settings.core;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.settings.core.Profile;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Profile.class)
public abstract class Profile_ {

	public static volatile SingularAttribute<Profile, String> profileName;
	public static volatile SingularAttribute<Profile, String> flag;
	public static volatile SingularAttribute<Profile, Long> profileId;
	public static volatile SingularAttribute<Profile, String> profileCode;
	public static volatile SingularAttribute<Profile, Date> createDate;

}

