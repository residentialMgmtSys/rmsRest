/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.service.facility;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rmsLib.dto.facility.EstateUnitDTO;
import org.javasoft.rmsRest.mapper.facility.EstateUnitMapper;
import org.javasoft.rmsRest.repository.facility.EstateUnitRepository;
import org.javasoft.rmsRest.service.CRUDServiceIntf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ayojava
 */
@Slf4j
@Service
@Transactional
public class EstateUnitService implements CRUDServiceIntf<EstateUnitDTO>{
    
    private final EstateUnitRepository estateUnitRepository;
    
    @Autowired
    private EstateUnitMapper estateUnitMapper; 
    
    @Autowired
    public EstateUnitService(EstateUnitRepository estateUnitRepository){
        this.estateUnitRepository = estateUnitRepository;
    }
    
    @Override
    public List<EstateUnitDTO> findAll(){
        return estateUnitMapper.convertEntityListToDTOList(estateUnitRepository.findAllBy());
    }

    @Override
    public EstateUnitDTO getById(Long id) {
        return estateUnitMapper.convertEntityToDTO(estateUnitRepository.getOne(id));
    }

    @Override
    public void delete(Long id) {
        estateUnitRepository.deleteById(id);
    }
}
