package org.javasoft.rmsRest.entity.user;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.settings.core.Menu;
import org.javasoft.rmsRest.entity.settings.core.Module;
import org.javasoft.rmsRest.entity.settings.core.Profile;
import org.javasoft.rmsRest.entity.user.UserProfile;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserProfile.class)
public abstract class UserProfile_ {

	public static volatile SingularAttribute<UserProfile, Profile> profile;
	public static volatile SingularAttribute<UserProfile, Module> module;
	public static volatile SingularAttribute<UserProfile, Long> userProfileId;
	public static volatile SingularAttribute<UserProfile, Menu> menu;
	public static volatile SingularAttribute<UserProfile, Date> createDate;

}

