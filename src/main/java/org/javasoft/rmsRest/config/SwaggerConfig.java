/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 https://springframework.guru/spring-boot-restful-api-documentation-with-swagger-2/
 */
package org.javasoft.rmsRest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author ayojava
 *  http://localhost:8080/swagger-ui.html#/
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any()).build().apiInfo(metaData());
    }

    private ApiInfo metaData() {
//        Contact contact = new Contact("Ayodeji Ilori", "https://gospelaccordingtojava.wordpress.com", "ayojava@gmail.com");
//        ApiInfo apiInfo = new ApiInfo("Spring Boot REST API","Spring Boot REST API for Online Store","1.0",
//                "TermsLib of service", contact, "Apache License Version 2.0", "https://www.apache.org/licenses/LICENSE-2.0",null);
//        return apiInfo;
    return new ApiInfoBuilder()
                .title("Resident Management System")
                .description("Resident Management System with Swagger")
                .termsOfServiceUrl("http://www.gospelaccordingtojava.wordpress.com")
                .contact("Ayodeji Ilori")
                .license("Apache License Version 2.0")
                .licenseUrl("https://github.com/ayojava")
                .version("2.0")
                .build();
    }
}
