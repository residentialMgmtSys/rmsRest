/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.service.settings.core;

import java.io.Serializable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.javasoft.rmsLib.dto.settings.core.ModuleDTO;
import org.javasoft.rmsRest.mapper.settings.core.ModuleMapper;
import org.javasoft.rmsRest.repository.settings.core.ModuleRepository;
import org.javasoft.rmsRest.service.CRUDServiceIntf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ayojava
 */
@Slf4j
@Service
@Transactional
public class ModuleService implements CRUDServiceIntf<ModuleDTO>{
    
    private final ModuleRepository moduleRepository;
    
    @Autowired
    private ModuleMapper moduleMapper; 
    
    @Autowired
    public ModuleService(ModuleRepository moduleRepository){
        this.moduleRepository = moduleRepository;
    }
    
    @Override
    public ModuleDTO getById(Long id) {
        return moduleMapper.convertEntityToDTO(moduleRepository.getOne(id));
    }

    @Override
    public List<ModuleDTO> findAll() {
        return moduleMapper.convertEntityListToDTOList(moduleRepository.findAllBy());
    }

    @Override
    public void delete(Long id) {
        moduleRepository.deleteById(id);
    }
}
