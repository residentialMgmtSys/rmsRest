/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.facility;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.javasoft.rmsRest.entity.facility.template.ResidentialAddressTemplate;
import org.javasoft.rmsRest.entity.occupants.RealResident;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@Table(name = "ResidentialUnit")
@EqualsAndHashCode(callSuper = true)
public class ResidentialUnit extends EstateUnit implements Serializable {

    @Column
    @Embedded
    private ResidentialAddressTemplate addressTemplate;
    
    @OneToMany(mappedBy = "residentialUnit",fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    private List<RealResident> realResidents;
    
    @Override
    public void setUnitSequence(int unitSequence){
        StringBuilder builder = new StringBuilder();
        builder = builder.append(addressTemplate.getBlockNum());
        builder = builder.append(StringUtils.leftPad(String.valueOf(addressTemplate.getFlatNum()), 1, "0"));
        unitSequence = Integer.parseInt(builder.toString());
    }
     
}
