package org.javasoft.rmsRest.entity.account;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.facility.EstateUnit;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EstateUnitAccount.class)
public abstract class EstateUnitAccount_ extends org.javasoft.rmsRest.entity.account.AbstractAccount_ {

	public static volatile SingularAttribute<EstateUnitAccount, EstateUnit> estateUnit;

}

