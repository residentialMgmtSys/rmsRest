package org.javasoft.rmsRest.entity.settings.core;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.settings.core.Functions;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Functions.class)
public abstract class Functions_ {

	public static volatile SingularAttribute<Functions, Boolean> newFn;
	public static volatile SingularAttribute<Functions, Boolean> deleteFn;
	public static volatile SingularAttribute<Functions, Boolean> editFn;
	public static volatile SingularAttribute<Functions, Boolean> viewFn;
	public static volatile SingularAttribute<Functions, Boolean> approvalFn;
	public static volatile SingularAttribute<Functions, Boolean> listFn;

}

