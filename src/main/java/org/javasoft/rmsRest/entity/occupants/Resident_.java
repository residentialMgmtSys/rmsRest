package org.javasoft.rmsRest.entity.occupants;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.account.ResidentAccount;
import org.javasoft.rmsRest.entity.flow.EntityState;
import org.javasoft.rmsRest.entity.group.ResidentGroup;
import org.javasoft.rmsRest.entity.occupants.Resident;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Resident.class)
public abstract class Resident_ {

	public static volatile SingularAttribute<Resident, Date> birthday;
	public static volatile SingularAttribute<Resident, String> lastName;
	public static volatile SingularAttribute<Resident, String> occupation;
	public static volatile SingularAttribute<Resident, Date> entryDate;
	public static volatile SingularAttribute<Resident, String> sex;
	public static volatile SingularAttribute<Resident, ResidentAccount> residentAccount;
	public static volatile SingularAttribute<Resident, String> altEmailAddress;
	public static volatile SingularAttribute<Resident, String> phoneNum;
	public static volatile SingularAttribute<Resident, String> title;
	public static volatile SingularAttribute<Resident, String> religion;
	public static volatile SingularAttribute<Resident, String> firstName;
	public static volatile SingularAttribute<Resident, String> emailAddress;
	public static volatile ListAttribute<Resident, ResidentGroup> residentGroups;
	public static volatile SingularAttribute<Resident, String> altPhoneNum;
	public static volatile SingularAttribute<Resident, EntityState> entityState;
	public static volatile SingularAttribute<Resident, String> identificationNo;
	public static volatile SingularAttribute<Resident, Long> residentId;
	public static volatile SingularAttribute<Resident, String> residentType;
	public static volatile SingularAttribute<Resident, String> maritalStatus;
	public static volatile SingularAttribute<Resident, Date> createDate;

}

