package org.javasoft.rmsRest.entity.facility;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.javasoft.rmsRest.entity.facility.template.ResidentialAddressTemplate;
import org.javasoft.rmsRest.entity.occupants.RealResident;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ResidentialUnit.class)
public abstract class ResidentialUnit_ extends org.javasoft.rmsRest.entity.facility.EstateUnit_ {

	public static volatile SingularAttribute<ResidentialUnit, ResidentialAddressTemplate> addressTemplate;
	public static volatile ListAttribute<ResidentialUnit, RealResident> realResidents;

}

