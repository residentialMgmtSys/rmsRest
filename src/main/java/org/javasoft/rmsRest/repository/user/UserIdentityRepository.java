/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.repository.user;

import java.util.Optional;
import java.util.stream.Stream;
import org.javasoft.rmsRest.entity.user.UserIdentity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ayojava
 */
@Repository
public interface UserIdentityRepository extends JpaRepository<UserIdentity,Long> , JpaSpecificationExecutor<UserIdentity>{
    
    Optional<UserIdentity> findByUserNameAndPasswordHash(String userName, String passwordHash);
    
    Stream<UserIdentity> findAllBy();
    
    @Modifying
    @Transactional(readOnly=false)
    @Query("update UserIdentity u set u.status =:status where u.userIdentityId = :userIdentityId ")
    int updateUserIdentityStatus(@Param("userIdentityId") Long userIdentityId,@Param("status") String status);
    
}
