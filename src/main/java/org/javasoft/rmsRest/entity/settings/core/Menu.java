/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.settings.core;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Immutable;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@Immutable
@NoArgsConstructor
public class Menu  implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long menuId;

    @NotEmpty
    @Column(name="menuCode")
    private String menuCode;
    
    @NotEmpty
    @Column(name="menuName")
    private String menuName;
    
    @NotEmpty
    private String flag; 
    
    @Column
    @Embedded
    private Functions functions;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false,name="createDate")
    @CreationTimestamp
    private Date createDate;
    
}
