/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.settings.core;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Immutable;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@Immutable
@NoArgsConstructor
@Table(name = "Modules")
public class Module  implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long moduleId;

    @NotEmpty
    @Column(name="moduleCode")
    private String moduleCode;

    @NotEmpty
    @Column(name="moduleName")
    private String moduleName;

    @NotEmpty
    private String flag;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinColumn(name = "profileId", nullable = false)
    private Profile profile;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "moduleId", referencedColumnName = "moduleId")
    private List<Menu> menus;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false,name="createDate")
    @CreationTimestamp
    private Date createDate;   
    
}
