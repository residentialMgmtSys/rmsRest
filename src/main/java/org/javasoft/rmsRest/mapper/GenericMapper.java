/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper;

import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author ayojava
 * @param <D> EntityDTO
 * @param <E> Entity
 */
public interface GenericMapper<D,E> {
    
   D convertEntityToDTO(E e);
   
   List<D> convertEntityListToDTOList(Stream<E> entities);
}
