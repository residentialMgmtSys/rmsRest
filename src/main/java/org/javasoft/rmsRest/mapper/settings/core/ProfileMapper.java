/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.mapper.settings.core;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.javasoft.rmsLib.dto.settings.core.ProfileDTO;
import org.javasoft.rmsRest.entity.settings.core.Profile;
import org.javasoft.rmsRest.mapper.GenericMapper;
import org.springframework.stereotype.Component;

/**
 *
 * @author ayojava
 */
@Component
public class ProfileMapper implements GenericMapper<ProfileDTO, Profile> {

    @Override
    public List<ProfileDTO> convertEntityListToDTOList(Stream<Profile> entities) {
        return entities.map(convertEntityToDTO).collect(Collectors.toList());
    }

    @Override
    public ProfileDTO convertEntityToDTO(Profile entity) {
        return convertEntityToDTO.apply(entity);
    }

    private Function<Profile, ProfileDTO> convertEntityToDTO = (Profile profile) -> {
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setCreateDate(profile.getCreateDate());
        profileDTO.setFlag(profile.getFlag());
        profileDTO.setProfileCode(profile.getProfileCode());
        profileDTO.setProfileId(profile.getProfileId());
        profileDTO.setProfileName(profile.getProfileName());
        return profileDTO;
    };
}
