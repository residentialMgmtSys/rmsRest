/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.mail;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.javasoft.rmsRest.entity.occupants.Resident;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "Email")
public class Email implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(generator = "EmailID_GEN", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "EmailID_GEN", sequenceName = "EmailID_SEQ")
    private Long emailId;

    private String mailSubject;
    
    private boolean attachment;

    private String attachmentFile;

    private boolean draft;//if true disable the option to select recipients from the front end, save recipient as the sender
    
    private String priorityType;//low, normal, high internal mails, different colours depict what to be displayed 
    
    private String status;//internal or external
        
    @Lob
    private String mailMessage;
    
    @OneToMany(mappedBy = "email" ,fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<EmailRecipients> emailRecipients ;
    
    @OneToOne(fetch = FetchType.EAGER)
    private Resident sender;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @CreationTimestamp
    private Date createDate;
}
