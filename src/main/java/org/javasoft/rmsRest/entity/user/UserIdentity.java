/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javasoft.rmsRest.entity.user;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;
import org.javasoft.rmsRest.entity.facility.EstateUnit;
import org.javasoft.rmsRest.entity.flow.EntityState;
import org.javasoft.rmsRest.entity.group.UserIdentityGroup;
import org.javasoft.rmsRest.entity.occupants.Resident;
import org.javasoft.rmsLib.utils.status.user.AbstractUserIdentityImpl;

/**
 *
 * @author ayojava
 */
@Data
@Entity
@NoArgsConstructor
@Table(name = "UserIdentity")
public class UserIdentity extends AbstractUserIdentityImpl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userIdentityId;
    
    @NotEmpty
    @Column(name = "userName",unique = true)
    private String userName;    
    
    @NotEmpty
    @Column(name = "passwordHash")
    private String passwordHash;
    
    @Column(nullable = false,unique = true,updatable = false,name = "systemNo")
    private String systemNo;
    
    private String flag;// Active & Inactive & Locked
    
    private String status;//Logged in / Logged out
    
    //display only users that are not sysadmin
    @Column(name = "sysAdmin")
    private boolean sysAdmin; 
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false,name = "createDate")
    @CreationTimestamp
    private Date createDate;
        
    @ManyToOne()
    @JoinTable(name = "UserIdentity_EstateUnit",
            joinColumns = @JoinColumn(name = "userIdentityId" ,referencedColumnName = "userIdentityId"),
            inverseJoinColumns = @JoinColumn(name = "estateUnitId" ,referencedColumnName = "estateUnitId"))
    private EstateUnit estateUnit;
    
    @JoinColumn(name = "residentId", referencedColumnName = "residentId", nullable = false)
    @OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @Fetch(value = FetchMode.SELECT)
    private Resident residentDetails;
    
    @JoinColumn(name = "entityStateId", referencedColumnName = "entityStateId", nullable = false)
    @OneToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @Fetch(value = FetchMode.SELECT)
    private EntityState entityState; // new , approved , rejected 
    
    @OneToMany(fetch = FetchType.EAGER,orphanRemoval = true)
    @Fetch(value = FetchMode.SELECT)
    private List<UserProfile> systemUserProfile;

    @ManyToMany(mappedBy = "allUserIdentity")
    private List<UserIdentityGroup> userIdentityGroups;
}
